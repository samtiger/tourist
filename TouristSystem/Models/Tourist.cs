﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TouristSystem.Models
{
    public class Tourist
    {
        public int Id { get; set; }
        public string Nationality { get; set; }
        public string MacAddress { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }

        [NotMapped]
        public List<int> PreferedSpecifications { get; set; }
    }
}