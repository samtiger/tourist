﻿namespace TouristSystem.Models
{
    public class Nationality
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double PreferableTemperature { get; set; }
    }
}