﻿using System.Data.Entity;

namespace TouristSystem.Models
{
    public class TouristContext : DbContext
    {
        public TouristContext() : base("TouristDb")
        {
        }

        public DbSet<Hardware> Hardwares { get; set; }
        public DbSet<Record> Records { get; set; }
        public DbSet<Tourist> Tourists { get; set; }
        public DbSet<Specification> Specefications { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Hardware>().HasMany(m => m.Specefications)
                .WithMany(e => e.Hardwares);
        }

    }
}