﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace TouristSystem.Models
{
    public class Hardware : IComparable<Hardware>
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string Image { get; set; }
        public double? Percentage { get; set; }

        [JsonIgnore]
        public virtual List<Record> Records { get; set; }
        public virtual List<Specification> Specefications { get; set; }

        public int CompareTo(Hardware other)
        {
            if (Percentage < other.Percentage)
                return -1;
            else if (Percentage > other.Percentage)
                return 1;
            else
            {
                return 0;
            }
        }

    }
}