﻿namespace TouristSystem.Models
{
    public class PlaceCount
    {
        public string HardwareId { get; set; }
        public string HardwareName { get; set; }
        public int Count { get; set; }
    }
}