﻿namespace TouristSystem.Models
{
    public class NationalityCount
    {
        public string Nationality { get; set; }
        public int Count { get; set; }
    }
}