﻿using System;

namespace TouristSystem.Models
{
    public class Record
    {
        public int Id { get; set; }
        public string HardwareId { get; set; }
        public Hardware Hardware { get; set; }
        public DateTime CreateDate { get; set; }
        public double Temperture { get; set; }
        public int Lux { get; set; }
        public int Humidity { get; set; }
        public int Count { get; set; }
    }
}