﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TouristSystem.Models
{
    public class Specification
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Hardware> Hardwares { get; set; }

    }
}