﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using TouristSystem.Models;

namespace TouristSystem.Controllers
{
    public class HomeController : Controller
    {
        private TouristContext db;

        public HomeController()
        {
            db = new TouristContext();
        }

        public ActionResult Download(string id)
        {
            var dir = Server.MapPath("/App_Data/uploads");
            var path = Path.Combine(dir, id + ".jpg");
            return base.File(path, "image/jpeg");
        }

        public ActionResult Index(DateTime? from, DateTime? to)
        {
            var records = db.Records.Where(m => (!from.HasValue || (m.CreateDate >= from.Value)) && (!to.HasValue || (m.CreateDate <= to.Value))).OrderByDescending(m => m.CreateDate).ToList();

            foreach (var record in records)
            {
                record.Hardware = db.Hardwares.First(m => m.Id == record.HardwareId);
            }

            return View(records);
        }

        public ActionResult GetPlacesCount()
        {
            var places = db.Hardwares.ToList();

            var tourists = db.Tourists.ToList();

            var list = GetPlacesTourists(places, tourists);

            return View(list);
        }

        private List<PlaceCount> GetPlacesTourists(List<Hardware> places, List<Tourist> tourists)
        {
            var result = new List<PlaceCount>();

            foreach (var place in places)
            {
                var placeCount = new PlaceCount
                {
                    HardwareId = place.Id,
                    HardwareName = place.Name,
                    Count = 0
                };

                foreach (var tourist in tourists)
                {
                    placeCount.Count += InRange(place.Latitude, place.Longitude, tourist.Latitude, tourist.Longitude)? 1 : 0;
                }
                result.Add(placeCount);
            }
            return result;
        }

        public ActionResult GetPlaceCount(string id)
        {
            var nationalityCount = new List<NationalityCount>();

            var place = db.Hardwares.FirstOrDefault(m => m.Id == id);

            var tourists = db.Tourists.ToList();

            var placeTourist = new List<Tourist>();

            foreach (var tourist in tourists)
            {
                if(InRange(place.Latitude, place.Longitude, tourist.Latitude, tourist.Longitude))
                {
                    placeTourist.Add(tourist);
                }
            }

            var touristNationalityGroups = placeTourist.GroupBy(m => m.Nationality);

            foreach (var touristNationalityGroup in touristNationalityGroups)
            {
                nationalityCount.Add(new NationalityCount
                {
                    Nationality = touristNationalityGroup.First().Nationality,
                    Count = touristNationalityGroup.Count()
                });
            }

            return View(nationalityCount);
        }

        private bool InRange(double lat1, double lng1, double lat2, double lng2)
        {
            var sCoord = new GeoCoordinate(lat1, lng1);
            var eCoord = new GeoCoordinate(lat2, lng2);

            return sCoord.GetDistanceTo(eCoord) <= 100;
        }

    }
}
