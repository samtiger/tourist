﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Web.Http;
using TouristSystem.Models;

namespace TouristSystem.Controllers
{
    public class RecordController : ApiController
    {
        private TouristContext db;
        private int idealCount = 20, idealHumidity = 50, idealLux = 1000;
        private double idealTemperature = 25;

        public RecordController()
        {
            db = new TouristContext();
        }

        [HttpGet]
        public IHttpActionResult AddRecord(string hardwareId, int lux, double temperture, int humidity, int count)
        {
            var record = new Record
            {
                Count = count,
                CreateDate = DateTime.Now,
                HardwareId = hardwareId,
                Humidity = humidity,
                Lux = lux,
                Temperture = temperture
            };

            db.Records.Add(record);
            db.SaveChanges();

            return Ok();
        }

        [HttpPost]
        public IHttpActionResult Process(Tourist tourist)
        {
            var dbTourist = db.Tourists.FirstOrDefault(m => m.MacAddress == tourist.MacAddress);
            if (dbTourist == null)
            {
                db.Tourists.Add(tourist);
            }
            else
            {
                dbTourist.Longitude = tourist.Longitude;
                dbTourist.Latitude = tourist.Latitude;
            }
            db.SaveChanges();

            var placeGroups = db.Records.GroupBy(m => m.HardwareId);

            var records = new List<Record>();

            foreach (var placeGroup in placeGroups)
            {
                var record = placeGroup.FirstOrDefault();
                record.Hardware = db.Hardwares.FirstOrDefault(m => m.Id == record.HardwareId);
                records.Add(record);
            }

            var result = ProcessViaFuzzyLogic(records, tourist.Latitude, tourist.Longitude, tourist.PreferedSpecifications);

            return Ok(result);
        }

        private Hardware[] ProcessViaFuzzyLogic(List<Record> records, double touristLatitude, double touristLongitude, List<int> preferedSpecifications)
        {
            var result = new Hardware[records.Count];
            var i = 0;
            foreach (var record in records)
            {
                record.Hardware.Percentage = EvaluateRecord(record, preferedSpecifications);
                result[i++] = record.Hardware;
            }

            Array.Sort(result);

            return result;
        }

        private double EvaluateRecord(Record record, List<int> specificationList)
        {
            var temperaturePercentage = EvaluateTemperature(record.Temperture);
            var humidityPercentage = EvaluateHumidity(record.Humidity);
            var luxPercentage = EvaluateLux(record.Lux);
            var countPercentage = EvaluateCount(record.Count);
            var specificationPercentage = EvaluateSpecification(record.Hardware.Specefications, specificationList);

            return (temperaturePercentage + humidityPercentage + luxPercentage + countPercentage + specificationPercentage) / 5;
        }

        private double EvaluateSpecification(List<Specification> specefications, List<int> specificationList)
        {
            var matchCount = 0.0;

            foreach (var specefication in specefications)
            {
                if (specificationList.Contains(specefication.Id))
                    matchCount++;
            }

            return matchCount / specificationList.Count;
        }

        private double EvaluateCount(int count)
        {
            var difference = Math.Abs(count - idealCount);

            // location has the ideal count of tourists
            if(difference == 0)
            {
                return 100;
            }

            // location is empty
            if(count <= 0 || count >= (idealCount * 2))
            {
                return 0;
            }

            if(difference <= 5)
            {
                return 75;
            }

            if(difference <= 10)
            {
                return 50;
            }

            return 25;
        }

        private double EvaluateLux(int lux)
        {
            return (lux / idealLux) * 100;
        }

        private double EvaluateHumidity(int humidity)
        {
            var difference = Math.Abs(humidity - idealHumidity);

            if(difference == 0)
            {
                return 100;
            }

            return 25;
        }

        private double EvaluateTemperature(double temperture)
        {
            var difference = Math.Abs(temperture - idealTemperature);

            // so cold or so hot
            if(temperture <= 0 || temperture >= 50)
            {
                return 0;
            }

            // ideal temperature
            if(difference == 0)
            {
                return 100;
            }

            if(difference <= 5)
            {
                return 75;
            }

            if(difference <= 10)
            {
                return 50;
            }

            return 25;
        }

        //private double GetDistance(double sourceLatitude, double sourceLongitude, double destinationLatitude, double destinationLongitude)
        //{
        //    var source = new GeoCoordinate(sourceLatitude, sourceLongitude);
        //    var destination = new GeoCoordinate(destinationLatitude, destinationLongitude);

        //    return source.GetDistanceTo(destination);
        //}

    }
}
